myApp.service("PeopleService", function ($http, $q) {
    this.getData = function(requestURL) {
        var deferred = $q.defer();

        console.log(requestURL);

        $http({
            method: 'GET',
            url: requestURL
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function errorCallback(response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    }
});