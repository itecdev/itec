var myApp = angular.module('iTEC', [
    'ui.router',
    'satellizer',
    'imageCropper',
    'infinite-scroll',
    'ui.bootstrap',
    'ngImgCrop'
]);

myApp.config(function($locationProvider, $stateProvider, $urlRouterProvider, $authProvider) {

    $authProvider.loginUrl = 'itec/api/login';

    // For any unmatched url, redirect to '/'
    $urlRouterProvider.otherwise("/404");

    // Now set up the states
    $stateProvider
        .state('index', {
            url: "/",
            templateUrl: "views/landing-page.html",
            controller: "AuthenticateController",
            data: {requiredLogin: false}
        })
        .state('events', {
            url: "/events",
            templateUrl: "views/events.html",
            controller: "EventsController",
            data: {requiredLogin: true}
        })
        .state('event', {
            url: "/event/:eventId",
            templateUrl: "views/event_details.html",
            controller: "ViewEventController",
            data: {requiredLogin: true}
        })
        .state('people', {
            url: "/people",
            templateUrl: "views/people.html",
            controller: "PeopleController",
            data: {requiredLogin: true}
        })
        .state('statistics', {
            url: "/statistics",
            templateUrl: "views/statistics.html",
            controller: "StatisticsController",
            data: {requiredLogin: true}
        })
        .state('user', {
            url: "/user/:userId",
            templateUrl: "views/user_profile.html",
            controller: "ViewUserController",
            data: {requiredLogin: true}
        })
        .state('404', {
            url: "/404",
            templateUrl: "views/errors/404.html",
            controller: "PeopleController",
            data: {requiredLogin: true}
        })
        .state('settings', {
            url: "/settings",
            templateUrl: "views/settings.html",
            controller: "SettingsController",
            data: {requiredLogin: true}
        })

    $locationProvider.html5Mode(true);
});

myApp.controller('MainController', function($rootScope, $scope, $http, $location, $rootScope, $auth, $state, ApplicationService) {
    $rootScope.location = $location;

    $rootScope.events = [];

    $rootScope.eventCover = {
        'uploadedCoverSRC' : '',
        'croppedImage' : ''
    };

    $rootScope.newEvent = {};
    $rootScope.newEvent.interests = [];
    $rootScope.alreadyJoinedEvent = {};
    $rootScope.tryToJoinEvent = {};

    $rootScope.newEventErrors = {
        'title' : '',
        'description' : '',
        'interests' : '',
        'startDate' : ''
    }

    $rootScope.continuePostingNewEvent = function() {
        var promise = ApplicationService.unjoinEvent('http://localhost/itec/api/unjoin-event', $scope.alreadyJoinedEvent.id);
        promise.then(function (data) {
            $scope.postEvent($scope.newEvent);
        });
        $('#already-joined-event-modal').modal('hide');
    }

    $rootScope.continueJoinEvent = function() {
        var promise = ApplicationService.unjoinEvent('http://localhost/itec/api/unjoin-event', $scope.alreadyJoinedEvent.id);
        promise.then(function (data) {
            $scope.joinEvent($scope.tryToJoinEvent);
        });
        $('#already-joined-event-modal2').modal('hide');
    }

    $rootScope.giveUpPostingNewEvent = function() {
        $rootScope.events = [];

        $rootScope.eventCover = {
            'uploadedCoverSRC' : '',
            'croppedImage' : ''
        };

        $rootScope.newEvent = {};
        $rootScope.newEvent.interests = [];
        $rootScope.alreadyJoinedEvent = {};

        $rootScope.newEventErrors = {
            'title' : '',
            'description' : '',
            'interests' : '',
            'startDate' : ''
        }

        $('#event-start-date').val('');

        $('#already-joined-event-modal').modal('hide');
    }

    $rootScope.giveUpJoinEvent = function() {
        $('#already-joined-event-modal2').modal('hide');
    }

    var dataURItoBlob = function(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
    };

    $rootScope.postEvent = function(event) {
        event.startDate = $('#event-start-date').val();

        var formData = new FormData();

        for(key in event) {
            if(key === 'documents') {
                angular.forEach(event[key], function(file) {
                    formData.append(key + '[]', file);
                })
            } else {
                formData.append(key, event[key]);
            }
        }

        // Append cover image if uploaded
        if($scope.eventCover.croppedImage != '') {
            var myBlob = dataURItoBlob($scope.eventCover.croppedImage);
            var originalNameArr = $scope.eventCover.uploadedCover.name.split('.');
            var originalName = '';
            for(var i=0; i<originalNameArr.length; i++) {
                if(i < originalNameArr.length-1) {
                    originalName += originalNameArr[i] + '.';
                }
            }
            originalName += 'png';

            formData.append('cover', myBlob, originalName);
        }

        $http({
            headers: {'Content-Type': undefined},
            method: 'POST',
            url: 'api/create-new-event',
            data: formData
        })
            .success(function (response, status, headers, config) {
                console.log('SUCCESS FUNCTION');
                console.log(response);

                if(response.success) {
                    $scope.events.push(response.event);

                    $scope.events = [];

                    $scope.eventCover = {
                        'uploadedCoverSRC' : '',
                        'croppedImage' : ''
                    };

                    $scope.newEvent = {};
                    $scope.newEvent.interests = [];
                    $scope.alreadyJoinedEvent = {};

                    $scope.newEventErrors = {
                        'title' : '',
                        'description' : '',
                        'interests' : '',
                        'startDate' : ''
                    }

                    $('#event-start-date').val('');

                    $('#open-new-event-container').click();
                }
            })
            .error(function (response, status, headers, config) {
                console.log('ERROR FUNCTION');
                console.log(response);

                if(status == 400) {
                    $scope.alreadyJoinedEvent = response.event;
                    $('#already-joined-event-modal').modal('show');
                }
            });
    }

    $rootScope.setJoinedStatus = function(event) {
        var response = false;

        angular.forEach(event.participants, function(participant) {
            if(participant.id == $rootScope.authUser.id) {
                response = true;
            }
        });

        return response;
    }

    $rootScope.joinEvent = function(event) {
        $http({
            method: 'POST',
            url: 'api/join-event',
            data: event.id
        })
            .success(function (response, status, headers, config) {
                console.log('SUCCESS FUNCTION');
                console.log(response);

                if(response.success) {
                    event.participants.push(response.data);
                    event.iJoined = true;
                }
            })
            .error(function (response, status, headers, config) {
                if(status == 400) {
                    $scope.tryToJoinEvent = event;
                    $scope.alreadyJoinedEvent = response.event;
                    $('#already-joined-event-modal2').modal('show');
                }

            });
    }

    $rootScope.unjoinEvent = function(event) {
        $http({
            method: 'POST',
            url: 'api/unjoin-event',
            data: event.id
        })
            .success(function (response, status, headers, config) {
                console.log('SUCCESS FUNCTION');
                console.log(response);

                if(response.success) {
                    event.participants = response.data;
                    event.iJoined = false;
                }
            })
            .error(function (response, status, headers, config) {
                console.log('ERROR FUNCTION');
                console.log(response);
            });
    }

    $(document).ready(function() {
        $.backstretch([
            "http://" + $location.host() + "/itec/img/landing_page_slider_02.jpg",
            "http://" + $location.host() + "/itec/img/landing_page_slider_03.jpg",
            "http://" + $location.host() + "/itec/img/landing_page_slider_01.jpg"
        ], {duration: 3000, fade: 750});
    });

    $scope.isUserLogged = function() {
        if($auth.isAuthenticated()) {
            return true;
        }

        return false;
    }

    $scope.logout = function () {
        $http({
            method: 'POST',
            url: 'api/logout'
        }).then(function successCallback(response) {
            $auth.logout();
            localStorage.removeItem('authUser');

            $state.go('index');
        }, function errorCallback(response) {

        });
    }
});

myApp.run(function($rootScope, $state, $auth, $http, $uibModalStack) {
    $uibModalStack.dismissAll();

    $rootScope.$on('$stateChangeStart', function(event, toState) {
        var requiredLogin = false;
        // check if this state need login
        if (toState.data && toState.data.requiredLogin) {
            requiredLogin = true;
        }

        // if yes and if this user is not logged in, redirect him to login page
        if (requiredLogin && !$auth.isAuthenticated()) {
            event.preventDefault();
            $state.go('index');
        }

        if (!requiredLogin && $auth.isAuthenticated()) {
            event.preventDefault();

            $rootScope.authUser = JSON.parse(localStorage.authUser);

            $state.go('events');
        }

        if(requiredLogin && $auth.isAuthenticated()) {
            $rootScope.authUser = JSON.parse(localStorage.authUser);
        }
    });
});

myApp.directive('dtStarRating', function(){
    return {
        template:
        '<ul class="rating-stars">'+
        '  <li ng-click="toggle($index)" ng-repeat="star in stars">'+
        '    <i ng-if="star.filled" class="glyphicon glyphicon-star"></i>'+
        '    <i ng-if="!star.filled" class="glyphicon glyphicon-star-empty"></i>'+
        '  </li>'+
        '</ul>',
        scope: {
            ratingValue: '=ngModel',
            onRatingSelect: '&?',
            max: '=?' // optional (default is 10)
        },
        link: function(scope, elem, attr) {

            if (scope.max == undefined) {
                scope.max = 10;
            }
            function updateStars() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function(index) {
                scope.ratingValue = index + 1;
                /*scope.onRatingSelect({
                 rating: index + 1
                 });*/
            };
            scope.$watch('ratingValue', function(oldValue, newValue) {
                if (newValue) {
                    updateStars();
                }
            });
        }
    };
});

myApp.directive("ngFileSelect", function() {
    return {
        link: function($scope, el) {
            el.bind("change", function(e) {
                $scope.getFile(e.target.files[0]);
            });
        }
    }
});

myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    values.push(item);
                });
                scope.$apply(function () {
                    if (attrs.multiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);

myApp.directive('imgSrcError', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.imgSrcError) {
                    attrs.$set('src', attrs.imgSrcError);
                }
            });

            attrs.$observe('ngSrc', function(value) {
                if (!value && attrs.imgSrcError) {
                    attrs.$set('src', attrs.imgSrcError);
                }
            });
        }
    }
});

myApp.service("ApplicationService", function ($rootScope, $http, $q) {
    this.unjoinEvent = function(requestURL, eventId) {
        var deferred = $q.defer();

        console.log(requestURL);

        $http({
            method: 'POST',
            url: requestURL,
            data: eventId
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function errorCallback(response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    }
});

myApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

myApp.directive('starRating', function () {
    return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&",
            mouseHover: "&",
            mouseLeave: "&"
        },
        restrict: 'EA',
        template:
            "<div style='display: inline-block; margin: 0px; padding: 0px; cursor:pointer;' ng-repeat='idx in maxRatings track by $index'> \
                    <img ng-src='{{((hoverValue + _rating) <= $index) && \"http://www.codeproject.com/script/ratings/images/star-empty-lg.png\" || \"http://www.codeproject.com/script/ratings/images/star-fill-lg.png\"}}' \
                    ng-Click='isolatedClick($index + 1)' \
                    ng-mouseenter='isolatedMouseHover($index + 1)' \
                    ng-mouseleave='isolatedMouseLeave($index + 1)'></img> \
            </div>",
        compile: function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        },
        controller: function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            $scope.$watch('rating', function(newValue, oldValue) {
                $scope.rating = $scope._rating = newValue;
            });

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;

            $scope.isolatedClick = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope.rating = $scope._rating = param;
                $scope.hoverValue = 0;
                $scope.click({
                    param: param
                });
            };

            $scope.isolatedMouseHover = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = 0;
                $scope.hoverValue = param;
                $scope.mouseHover({
                    param: param
                });
            };

            $scope.isolatedMouseLeave = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = $scope.rating;
                $scope.hoverValue = 0;
                $scope.mouseLeave({
                    param: param
                });
            };
        }
    };
});

myApp.directive("ngFileSelect", function() {
    return {
        link: function($scope, el) {
            el.bind("change", function(e) {
                $scope.getFile(e.target.files[0]);
            });
        }
    }
});