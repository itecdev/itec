myApp.service("ViewEventService", function ($http, $q) {
    this.getData = function(requestURL, eventId) {
        var deferred = $q.defer();

        console.log('ViewEventService');
        console.log(requestURL);

        console.log('eventId = ' + eventId);

        $http({
            method: 'POST',
            url: requestURL,
            data: eventId
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function errorCallback(response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    }
});