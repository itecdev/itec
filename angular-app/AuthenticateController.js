myApp.controller('AuthenticateController', function($scope, $http, $location, $rootScope, $auth, $state) {
	$scope.user = {};
	$scope.succesfulRegistration = false;

	$scope.login = function() {
		console.log($scope.user);

		var credentials = {
			email: $scope.user.email,
			password: $scope.user.password
		};

		console.log(credentials);

		// Use Satellizer's $auth service to login
		$auth.login(credentials).then(function() {

			return $http.post('api/get-logged-user');

		}, function(error) {
			console.log(error);
			$scope.loginError = true;
			$scope.loginErrorText = error.data.error;

		}).then(function(response) {
			if(response != undefined) {
				$rootScope.authUser = response.data.user;
				localStorage.setItem("authUser", JSON.stringify($rootScope.authUser));

				$scope.loginError = false;
				$scope.loginErrorText = '';

				$state.go('events');
			}
		});
	};

	var register = function() {
		console.log($scope.user);

		$http({
			method: 'POST',
			url: '/itec/api/register',
			data: $scope.user
		})
			.success(function (response, status, headers, config) {
				console.log('success');
				console.log(response);
				$scope.succesfulRegistration = true;
			})
			.error(function (response, status, headers, config) {
				console.log('error');
				console.log(response);
			});
	}

	$scope.submitRegisterForm = function(isValid) {
		if(isValid) {
			register();
		}
	};
});