myApp.controller('ViewEventController', function($scope, $http, $location, $rootScope, $stateParams, ViewEventService) {
    $scope.event = {};
    $scope.event.rating = 0;

    $scope.myComment = {
        'message' : '',
        'attachments' : [],
        'edit' : false
    }

    var init = function() {
        var promise = ViewEventService.getData('http://'+$location.host()+'/itec/api/get-event', $stateParams.eventId);
        promise.then(function (data) {
            $scope.event = data.event;

            console.log($scope.event);

            $scope.event.iCommented = false;
            angular.forEach($scope.event.comments, function(comment) {
                if(comment.author.id == $rootScope.authUser.id) {
                    $scope.event.iCommented = true;
                }
            });

            $scope.event.iJoined = $scope.setJoinedStatus($scope.event);
        });
    }

    $(document).ready(function() {
        init();
    });

    $scope.saveComment = function(comment) {
        $scope.myComment.message = comment.message;
        $scope.postComment();
    }

    $scope.postComment = function() {
        var formData = new FormData();

        for(key in $scope.myComment) {
            if(key === 'attachments') {
                angular.forEach($scope.myComment[key], function(file) {
                    formData.append(key + '[]', file);
                })
            } else {
                formData.append(key, $scope.myComment[key]);
            }
        }

        formData.append('eventId', $scope.event.id);

        $http({
            headers: {'Content-Type': undefined},
            method: 'POST',
            url: 'api/post-comment',
            data: formData
        })
            .success(function (response, status, headers, config) {
                console.log('SUCCESS FUNCTION');
                console.log(response);

                if(response.success) {
                    if(response.comment) {
                        $scope.event.comments.push(response.comment);
                        $scope.event.iCommented = true;
                    }

                    $scope.myComment = {
                        'message' : '',
                        'attachments' : [],
                        'edit' : false
                    }
                }
            })
            .error(function (response, status, headers, config) {
                console.log('ERROR FUNCTION');
                console.log(response);
            });
    }

    $scope.triggerUpload = function() {
        var fileuploader = angular.element("#my-comment-attachments");

        fileuploader.on('click', function() {
        });

        fileuploader.trigger('click')
    }

    $scope.submitRating = function(param) {
        $http({
            method: 'POST',
            url: '/itec/api/rate-event',
            data: {
                'rating' : param,
                'eventId' : $scope.event.id
            }
        })
            .success(function (response, status, headers, config) {
                if(response.success) {
                    $scope.event.rating = response.rating;
                }
            })
            .error(function (response, status, headers, config) {
                console.log('error');
                console.log(response);
            });
    }

    $scope.getInterestsOfEvent = function(event) {
        if(event != undefined && event.interests != undefined) {
            return event.interests.split(',');
        }
        return {};
    }
});