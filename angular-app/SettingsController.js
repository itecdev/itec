myApp.controller('SettingsController', function($scope, $http, $location, $rootScope, fileReader) {
    console.log('authUser');
    console.log($scope.authUser);

    $scope.init = function() {
        $scope.newUserModel = {
            id : $scope.authUser.id,
            firstname: $scope.authUser.firstname,
            lastname: $scope.authUser.lastname,
            profile_icon: $scope.authUser.profile_icon,
            email: $scope.authUser.email,
            interests: $scope.authUser.interests != null && $scope.authUser.interests.length != '' ? $scope.authUser.interests.split(',') : [],
            changePassword: false,
            oldPassword: '',
            newPassword: '',
            iconAccepted: true
        };

        $scope.uploadedAvatar = '';
        $scope.myCroppedImage = '';
        $scope.avatarSrc = '';
        $scope.initialAvatar = $scope.authUser.profile_icon;
        $scope.profileChangesControl = false;    
    }

    $scope.init();

    $scope.errors = {};

    $scope.triggerUpload = function () {
        var fileuploader = angular.element("#upload-avatar");

        fileuploader.on('click', function () {
        });

        fileuploader.trigger('click')
    };

    $scope.getFile = function (file) {
        $scope.newUserModel.iconAccepted = false;
        $scope.uploadedAvatar = file;
        fileReader.readAsDataUrl($scope.uploadedAvatar, $scope)
            .then(function (result) {
                $scope.avatarSrc = result;
            });
    };

    $scope.acceptProfileIcon = function (newAvatar) {
        $scope.myCroppedImage = $('#croppedImg').attr('src');
        $('#upload-avatar').val('');
        $scope.newUserModel.iconAccepted = true;
        $scope.initialAvatar = $scope.myCroppedImage;

        $scope.newUserModel.myCroppedImage = $scope.myCroppedImage;
        $scope.newUserModel.uploadedAvatar = $scope.uploadedAvatar;

        $scope.profileChangesControl = true;
    };

    $scope.cancelProfileIcon = function () {
        $scope.myCroppedImage = '';
        $('#upload-avatar').val('');
        $scope.uploadedAvatar = '';
        $scope.avatarSrc = '';
        $scope.newUserModel.iconAccepted = true;
    };

    $scope.profileChangesEvent = function () {
        $scope.profileChangesControl = true;
    };

    $scope.createNewInterest = function(newInterest) {
        if(newInterest.trim() != '') {
            if($scope.newUserModel.interests.indexOf(newInterest) == -1) {
                $scope.newUserModel.interests.push(newInterest);
                $scope.newInterest = '';
                $scope.errors.newInterest = '';
                $scope.profileChangesEvent();
            } else {
                $scope.errors.newInterest = 'You already have this interest!';
            }
        }
    }

    $scope.removeInterest = function(interest) {
        $scope.newUserModel.interests.splice($scope.newUserModel.interests.indexOf(interest), 1);
        $scope.profileChangesEvent();
    }

    var dataURItoBlob = function (dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
    };

    $scope.saveProfile = function () {
        var formData = new FormData();

        for (key in $scope.newUserModel) {
            if (key != 'uploadedAvatar') {
                if (key == 'myCroppedImage') {
                    var myBlob = dataURItoBlob($scope.newUserModel.myCroppedImage);
                    var originalNameArr = $scope.newUserModel.uploadedAvatar.name.split('.');
                    var originalName = '';
                    for (var i = 0; i < originalNameArr.length; i++) {
                        if (i < originalNameArr.length - 1) {
                            originalName += originalNameArr[i] + '.';
                        }
                    }
                    originalName += 'png';

                    formData.append('file', myBlob, originalName);
                } else {
                    formData.append(key, $scope.newUserModel[key]);
                }
            }
        }

        if ($scope.uploadedAvatar != '') {
            var myBlob = dataURItoBlob($scope.myCroppedImage);
            var originalNameArr = $scope.uploadedAvatar.name.split('.');
            var originalName = '';
            for (var i = 0; i < originalNameArr.length; i++) {
                if (i < originalNameArr.length - 1) {
                    originalName += originalNameArr[i] + '.';
                }
            }
            originalName += 'png';

            formData.append('file', myBlob, originalName);
        }

        $http({
            headers: {'Content-Type': undefined},
            method: 'POST',
            url: 'http://localhost/itec/api/save-profile',
            data: formData
        })
        .success(function (data, status, headers, config) {
            if (data.success) {
                $rootScope.authUser = data.editedUser;

                // update local storage
                if(localStorage.getItem('authUser')) {
                    localStorage.removeItem('authUser');
                    localStorage.setItem('authUser', JSON.stringify(data.editedUser));
                }

                $scope.init();
            }
        })
        .error(function (data, status, headers, config) {
            console.log(data);
        });
    };
});