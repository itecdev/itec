myApp.controller('PeopleController', function($scope, $http, $location, $rootScope, $stateParams, PeopleService) {
    $scope.users = {};
    var init = function() {
        var promise = PeopleService.getData('http://localhost/itec/api/get-all-users');
        promise.then(function (data) {
            console.log(data);
            $scope.users = data.users;
            console.log($scope.users);
        });
    }

    $(document).ready(function() {
        init();
    });
});