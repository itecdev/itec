myApp.controller('ViewUserController', function($scope, $http, $location, $rootScope, $stateParams, ViewUserService) {
    $scope.user = {};

    var init = function() {
        var promise = ViewUserService.getData('http://'+$location.host()+'/itec/api/get-user', $stateParams.userId);
        promise.then(function (data) {
            console.log(data);
            $scope.user = data.user;

            if($scope.user.interests != null) {
                $scope.user.interests = $scope.user.interests.split(',');
            }
            
            console.log($scope.user);

            angular.forEach($scope.user.events, function(event) {
                event.iJoined = $scope.setJoinedStatus(event);
            });
        });
    }

    $(document).ready(function() {
        init();
    });
});