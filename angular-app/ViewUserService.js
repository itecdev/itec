myApp.service("ViewUserService", function ($http, $q) {
    this.getData = function(requestURL, userId) {
        var deferred = $q.defer();

        console.log('ViewUserService');
        console.log(requestURL);

        console.log('userId = ' + userId);

        $http({
            method: 'POST',
            url: requestURL,
            data: userId
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function errorCallback(response) {
            deferred.resolve(response);
        });

        return deferred.promise;
    }
});