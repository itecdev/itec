myApp.controller('EventsController', function($scope, $state, $http, $location, $rootScope, fileReader, EventsService, ApplicationService) {
	var socket = io.connect('localhost:8890');

	$scope.viewEventList = 'all';
	$scope.firstIndex = 0;
	$scope.stopInfiniteScroll = false;

	$scope.myButtonLabelsObject = {
		rotateLeft: '',
		rotateLeft: '',
		rotateRight: '',
		zoomIn: 'Zoom in',
		zoomOut: 'Zoom out',
		fit: 'Fit',
		crop: 'Crop' // You can pass html too.
	}

	socket.on("real-time-event:App\\Events\\JoinEvent", function (data) {
		console.log(data);
	});

	socket.on("real-time-event:App\\Events\\NewEvent", function (data) {
		console.log(data);

		$scope.events.push(data.event);
		$scope.$apply();

		//$scope.sortEventsList();
	});

	$scope.createNewInterest = function(newInterest) {
		if(newInterest != undefined && newInterest != '') {
			var control = false;
			angular.forEach($scope.newEvent.interests, function(interest) {
				if(interest == newInterest) {
					control = true;
				}
			})

			if(!control) {
				$scope.newEvent.interests.push(newInterest);
				$scope.newInterest = '';
			}
		}
	}

	$scope.removeInterest = function(interest) {
		$scope.newEvent.interests.splice($scope.newEvent.interests.indexOf(interest), 1);
	}

    var init = function() {
        var promise = EventsService.getData('http://localhost/itec/api/get-all-events');
        promise.then(function (data) {
            console.log(data);
            $scope.events = data.events;
            console.log($scope.events);

            angular.forEach($scope.events, function(event) {
            	event.iJoined = $scope.setJoinedStatus(event);
            });
        });
    }
	
	$(document).ready(function() {
		init();

		if(window.File && window.FileReader && window.FileList && window.Blob) {
			function handleFileSelect(evt) {
				evt.stopPropagation();
				evt.preventDefault();

				var files = evt.dataTransfer.files; // FileList object.

				$scope.eventCover.uploadedCover = files[0];
				$scope.getFile($scope.eventCover.uploadedCover);

				$('#upload-cover').val('');
			}

			function handleDragOver(evt) {
				evt.stopPropagation();
				evt.preventDefault();
			}

			// Setup the dnd listeners.
			var dropZone = document.getElementById('dropZone');
			dropZone.addEventListener('dragover', handleDragOver, false);
			dropZone.addEventListener('drop', handleFileSelect, false);
		} else {
			window.alert('The File APIs are not fully supported in this browser.');
		}

		$("#event-start-date").on("dp.change", function (e) {
			window.alert('a');
			$('#event-start-date').data("DateTimePicker").setMinDate(e.date);
		});
	});

	$scope.getFile = function(file) {
		$scope.eventCover.uploadedCover = file;

		fileReader.readAsDataUrl($scope.eventCover.uploadedCover, $scope).then(function(result) {
			$scope.eventCover.uploadedCoverSRC = result;
		});
	};

	$scope.updateResultImage  = function(base64) {
		$scope.eventCover.croppedImage = base64;
		$scope.eventCover.uploadedCoverSRC = '';
		$scope.$apply();

		$('#upload-cover').val('');
		$('#dropZone').css('padding', 0);
	}

	$scope.triggerCoverUpload = function() {
		var fileuploader = angular.element("#upload-cover");

		fileuploader.on('click', function() {
		});

		fileuploader.trigger('click')
	};

	$scope.triggerDocumentsUpload = function() {
		var fileuploader = angular.element("#upload-documents");

		fileuploader.on('click', function() {
		});

		fileuploader.trigger('click')
	};

	$scope.validatePostEventForm = function(event) {
		var control = true;

		console.log('event');
		console.log(event);

		if(event.title == undefined || event.title == '') {
			control = false;
			$scope.newEventErrors.title = 'Required!';
		}
		if(event.description == undefined || event.description == '') {
			control = false;
			$scope.newEventErrors.description = 'Required!';
		}
		if(event.interests.length == 0) {
			control = false;
			$scope.newEventErrors.interests = 'Required!';
		}
		if($('#event-start-date').val() == '') {
			control = false;
			$scope.newEventErrors.startDate = 'Invalid date!';
		}

		if(control) {
			$scope.postEvent(event);
		}
	}

	$scope.setParticipants = function(event) {
		$scope.selectedParticipantsList = event.participants;
	}

	$scope.viewUserProfile = function(userId) {
		$('#participants-modal').modal('hide');


		$location.path("user/" + userId);
	}

	$scope.sortEventsList = function() {
		if($scope.viewEventList == 'all') {
			sortByRecommandations();
		}

		if($scope.viewEventList == 'best_rated') {
			sortByBestRated();
		}
	}

	var sortByRecommandations = function() {
		if($rootScope.authUser.interests != null && $rootScope.authUser.interests.split(',').length != 0) {
			for(var i=0; i<$scope.events.length-1; i++) {
				for(var j=i+1; j<$scope.events.length; j++) {
					if(fitness($scope.events[i]) < fitness($scope.events[j])) {
						var aux = $scope.events[i];
						$scope.events[i] = $scope.events[j];
						$scope.events[j] = aux;
					}
				}
			}
		}
	}

	var fitness = function(obj) {
		return getCommonInterestsNumber($rootScope.authUser.interests, obj.interests) * 
			anticipatedRating($rootScope.authUser, obj);
	}

	var getCommonInterestsNumber = function(listA, listB) {
		var response = 0;

		angular.forEach(listA.split(','), function(elA) {
			angular.forEach(listB.split(','), function(elB) {
				if(elA == elB) {
					response++;
				}
			})
		});

		return response;
	}

	var anticipatedRating = function(lUser, event) {
		var a = 0;
		for(var i=0; i<event.ratings.length; i++) {
			if(lUser.interests.split(',').length == 0) {
				a += 0;
			} else {
				a += (getCommonInterestsNumber(lUser.interests, event.ratings[i].user.interests) * 
					event.ratings[i].value) / lUser.interests.split(',').length;
			}
		}

		var b = 0;
		for(var i=0; i<event.ratings.length; i++) {
			if(lUser.interests.split(',').length == 0) {
				b += 0;
			} else {
				b += getCommonInterestsNumber(lUser.interests, event.ratings[i].user.interests) /
				lUser.interests.split(',').length;
			}
		}

		if(b == 0) return 0;

		return a / b;	
	}

	var sortByBestRated = function() {
		for(var i=0; i<$scope.events.length-1; i++) {
			for(var j=i+1; j<$scope.events.length; j++) {
				if($scope.events[i].rating < $scope.events[j].rating) {
					var aux = $scope.events[i];
					$scope.events[i] = $scope.events[j];
					$scope.events[j] = aux;
				}
			}
		}
	}

	$scope.shouldISeeThis = function(event) {
		if($scope.viewEventList == 'my_events') {
			var control = false;
			angular.forEach(event.participants, function(participant) {
				if(participant.id == $rootScope.authUser.id) {
					control = true;
				}
			})
			return control;
		}

		return true;
	}

	$scope.getInterestsOfEvent = function(event) {
		if(event != undefined && event.interests != undefined) {
			return event.interests.split(',');
		}
		return {};
	}
});