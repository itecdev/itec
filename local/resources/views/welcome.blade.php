<!DOCTYPE html>
<html ng-app="iTEC" ng-controller="MainController">
    <head>
        <base href="/itec/">
        <title>Unified Tastes</title>
        <link rel="shortcut icon" href="{{asset('/img/unified_post_favicon.png')}}}">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('/css/lib/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/lib/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/itec.css')}}">
        <!-- JavaScript Libraries -->
        <script src="{{asset('/js/lib/jquery.min.js')}}"></script>
        <script src="{{asset('/js/lib/jquery.backstretch.min.js')}}"></script>
        <script src="{{asset('/js/lib/bootstrap.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-animate.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-resource.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-route.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-sanitize.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-ui-router.min.js')}}"></script>
        <script src="{{asset('/js/lib/angular-image-cropper.js')}}"></script>
        <script src="{{asset('/js/lib/satellizer.min.js')}}"></script>
        <script src="{{asset('/js/lib/ng-infinite-scroll.min.js')}}"></script>
        <script src="{{asset('/js/lib/moment.min.js')}}"></script>
        <script src="{{asset('/js/lib/socket.io.js')}}"></script>
        <script src="{{asset('/js/lib/ui-bootstrap-tpls-1.3.2.min.js')}}"></script>
        <script src="{{asset('/js/lib/bootstrap-datetimepicker.min.js')}}"></script>
        <!-- JavaScript Custom -->
        <script src="{{asset('/angular-app/app.js')}}"></script>
        <script src="{{asset('/angular-app/EventsController.js')}}"></script>
        <script src="{{asset('/angular-app/EventsService.js')}}"></script>
        <script src="{{asset('/angular-app/AuthenticateController.js')}}"></script>
        <script src="{{asset('/angular-app/SettingsController.js')}}"></script>
        <script src="{{asset('/angular-app/PeopleController.js')}}"></script>
        <script src="{{asset('/angular-app/PeopleService.js')}}"></script>
        <script src="{{asset('/angular-app/ViewEventController.js')}}"></script>
        <script src="{{asset('/angular-app/ViewEventService.js')}}"></script>
        <script src="{{asset('/angular-app/ViewUserController.js')}}"></script>
        <script src="{{asset('/angular-app/ViewUserService.js')}}"></script>
        <script src="{{asset('/js/lib/ng-img-crop.js')}}"></script>
        <script src="{{asset('/js/lib/upload.js')}}"></script>
    </head>
    <body>
        <div ng-if="isUserLogged()" ng-include="'views/header.html'"></div>
        <div ui-view></div>

        <div ng-include="'views/modals/already_joined_event.html'"></div>
        <div ng-include="'views/modals/already_joined_event2.html'"></div>
    </body>
</html>