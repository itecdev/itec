<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItecDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('title');
			$table->string('cover');
            $table->text('description');
            $table->text('interests');
            $table->dateTime('starting_date');
            $table->tinyInteger('expired')->default(0);
            $table->timestamps();
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->text('message');
            $table->timestamps();
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('url');
            $table->timestamps();
        });

        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->enum('value', ['1', '2', '3', '4', '5'])->default('3');
            $table->timestamps();
        });

        Schema::create('user_on_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('event_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('comment_on_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('comment_id');
            $table->unsignedInteger('event_id');
            $table->timestamps();

            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('attachment_on_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attachment_id');
            $table->unsignedInteger('event_id');
            $table->timestamps();

            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('attachment_on_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attachment_id');
            $table->unsignedInteger('comment_id');
            $table->timestamps();

            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('rating_on_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rating_id');
            $table->unsignedInteger('event_id');
            $table->timestamps();

            $table->foreign('rating_id')->references('id')->on('ratings')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_on_event');
        Schema::dropIfExists('attachment_on_comment');
        Schema::dropIfExists('attachment_on_event');
        Schema::dropIfExists('comment_on_event');
        Schema::dropIfExists('user_on_event');
        Schema::dropIfExists('ratings');
        Schema::dropIfExists('attachments');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('events');
    }
}
