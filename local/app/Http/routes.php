<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/api/register', 'AuthenticateController@register');
Route::post('/api/login', 'AuthenticateController@authenticate');
Route::post('/api/logout', 'AuthenticateController@logout');

Route::post('/api/get-logged-user', 'UserController@getLoggedUser');
Route::post('/api/create-new-event', 'UserController@createNewEvent');
Route::get('/api/get-all-users', 'UserController@getAllUsers')->middleware('jwt.auth');
Route::get('/api/test', 'UserController@test')->middleware('jwt.auth');
Route::post('/api/get-all-events', 'UserController@getAllEvents')->middleware('jwt.auth');
Route::get('/api/test', 'UserController@test');
Route::post('/api/join-event', 'UserController@joinEvent')->middleware('jwt.auth');
Route::post('/api/unjoin-event', 'UserController@unjoinEvent')->middleware('jwt.auth');
Route::post('/api/get-event', 'UserController@getEventById')->middleware('jwt.auth');
Route::post('/api/get-user', 'UserController@getUserById')->middleware('jwt.auth');
Route::post('/api/post-comment', 'UserController@postComment')->middleware('jwt.auth');
Route::post('/api/rate-event', 'UserController@rateEvent')->middleware('jwt.auth');
Route::post('/api/save-profile', 'UserController@saveProfile')->middleware('jwt.auth');
