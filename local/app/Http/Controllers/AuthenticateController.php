<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Validator;
use Log;

class AuthenticateController extends Controller
{
    public function register(Request $request) {
        Log::info($request->all());

        // Validate inputs

        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|min:2',
            'lastname' => 'required|string|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/|min:8'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => true
            ], 400);
        }

        // Save user in database
        User::create([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);

        return response()->json([
            'success' => true
        ], 200);
    }

    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function logout(){
        JWTAuth::invalidate(JWTAuth::getToken());
    }
}
