<?php

namespace App\Http\Controllers;

use App\Events\JoinEvent;
use App\Events\NewEvent;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Requests;

use App\User;
use App\Event;
use App\Attachment;
use App\Comment;
use App\Rating;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Log;
use Validator;
use Carbon\Carbon;

class UserController extends Controller
{
	public function __constructor() {
		$this->middleware('jwt.auth');
	}

	public function getLoggedUser(Request $request) {
		Log::info('getLoggedUser');
		Log::info($request->headers);

		$authUser = JWTAuth::parseToken()->toUser();

		return response()->json([
			'success' => true,
			'user' => $authUser
		], 200);
	}

	public function createNewEvent(Request $request) {
		$validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'startDate' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => true
            ], 400);
        }

        // Check if I didn't join other event in this time
        $newEventStartingDate = Carbon::parse($request->input('startDate'));

        $authUser = JWTAuth::parseToken()->toUser();
        $joinedEvents = $authUser->events()->get();

        foreach($joinedEvents as $joinedEvent) {
            $timeDifference = $newEventStartingDate->diffInSeconds(Carbon::parse($joinedEvent->starting_date));
            Log::info($timeDifference);

            if($timeDifference < 3600 * 3) {
                return response()->json([
                    'success' => true,
                    'event' => $joinedEvent
                ], 400);
            }
        }

        // Create event
        $event = new Event();
        $event->title = $request->input('title');
        $event->description = $request->input('description');
        $event->starting_date = Carbon::parse($request->input('startDate'));
        $event->owner()->associate(JWTAuth::parseToken()->toUser());
        $event->interests = $request->input('interests');

        if($request->exists('cover')) {
            $attachmentURL = $this->uploadFile($request->file('cover'));

            if($attachmentURL == null) {
                return response()->json([
                    'success' => true
                ], 400);
            } else {
                $event->cover = $attachmentURL;
            }
        }

        $event->save();

        // Add owner as a participant
        $event->participants()->attach(JWTAuth::parseToken()->toUser());
        $event->save();

        // Add attachments
        if($request->exists('documents')) {
            foreach($request->file('documents') as $attachment) {
                $attachmentURL = $this->uploadFile($attachment);

                if($attachmentURL == null) {
                    return response()->json([
                        'success' => true
                    ], 400);
                } else {
                    $attachment = new Attachment();
                    $attachment->uploader()->associate(JWTAuth::parseToken()->toUser());
                    $attachment->url = $attachmentURL;
                    $attachment->save();

                    $event->attachments()->attach($attachment);
                    $event->save();
                }
            }
        }

		event(new NewEvent($event));

        return response()->json([
            'success' => true,
            'event' => $event
        ], 200);
	}

	public function getAllUsers(Request $request) {
		Log::info($request->headers);
		$users = User::all();

		Log::info($users);
		return response()->json([
			'succes' => true,
			'users' => $users
		], 200);
	}

	public function getAllEvents(Request $request) {
        $events = Event::where('expired', '==', 0)->with(
            'participants',
            'ratings.user'
        )->get();

        foreach($events as $key => $event) {
            $events[$key]['rating'] = 0;
        }

        foreach($events as $key0 => $event) {
            foreach($event['ratings'] as $key1 => $rating) {
                $events[$key0]['rating'] += $rating['value'];
            }
        }

        foreach($events as $key => $event) {
            if($event['rating'] != 0) {
                $events[$key]['rating'] = round($events[$key]['rating'] / count($event['ratings']));
            }
        }

		return response()->json([
			'succes' => true,
			'events' => $events
		], 200);
	}

    public function getEventById(Request $request) {
        $event = Event::where('id', '=', $request->all()[0])->with(
            'participants',
            'comments.attachments',
            'comments.author',
            'attachments',
            'ratings'
        )->get()[0];

        $event->rating = 0;
        foreach($event->ratings as $rating) {
            $event->rating += $rating->value;
        }

        if($event->rating != 0) {
            $event->rating = round($event->rating / count($event->ratings));
        }

        return response()->json([
            'success' => true,
            'event' => $event
        ], 200);
    }

    public function rateEvent(Request $request) {
        $ratings = Rating::whereHas('user', function($query) {
            $query->where('id', '=', JWTAuth::parseToken()->toUser()->id);
        })->whereHas('events', function($query) use($request) {
            $query->where('event_id', '=', $request->input('eventId'));
        })->get();

        if($ratings->isEmpty()) {
            $rating = new Rating();
            $rating->user()->associate(JWTAuth::parseToken()->toUser());
            $rating->value = $request->input('rating');
            $rating->save();

            $rating->events()->attach(Event::findOrFail($request->input('eventId')));
            $rating->save();
        } else {
            // update rating
            $ratings[0]->value = $request->input('rating');
            $ratings[0]->save();
        }

        // Return rating
        $event = Event::where('id', '=', $request->input('eventId'))->with('ratings')->get()[0];

        $event->rating = 0;
        foreach($event->ratings as $rating) {
            $event->rating += $rating->value;
        }

        if($event->rating != 0) {
            $event->rating = round($event->rating / count($event->ratings));
        }

        Log::info('rating '.$event->rating);

        return response()->json([
            'success' => true,
            'rating' => $event->rating
        ], 200);
    }

    public function getUserById(Request $request) {
        $users = User::where('id', '=', $request->all()[0])->with(
            'events.participants',
            'events.ratings'
        )->get()[0];

        foreach ($users->events as $event) {
            $collection = collect($event->ratings);
            $event['rating'] = $collection->avg('value');
        }

        return response()->json([
            'success' => true,
            'user' => $users
        ], 200);
    }

    public function joinEvent(Request $request) {
        $event = Event::findOrFail($request->all()[0]);

        $eventStartingDate = Carbon::parse($event->starting_date);

        $authUser = JWTAuth::parseToken()->toUser();
        $joinedEvents = $authUser->events()->get();

        foreach($joinedEvents as $joinedEvent) {
            $timeDifference = $eventStartingDate->diffInSeconds(Carbon::parse($joinedEvent->starting_date));
            Log::info($timeDifference);

            if($timeDifference < 3600 * 3) {
                return response()->json([
                    'success' => true,
                    'event' => $joinedEvent
                ], 400);
            }
        }

        $event->participants()->attach(JWTAuth::parseToken()->toUser());

        $response = $event->participants()->where('user_id', '=', JWTAuth::parseToken()->toUser()->id);

        event(new JoinEvent($response->get()));

        return response()->json([
            'success' => true,
            'data' => $response
        ], 200);
    }

    public function unjoinEvent(Request $request) {
        $event = Event::findOrFail($request->all()[0]);

        $event->participants()->detach(JWTAuth::parseToken()->toUser());

        $response = $event->participants()->get();

        return response()->json([
            'success' => true,
            'data' => $response
        ], 200);
    }

    public function postComment(Request $request) {
        Log::info($request->all());

        $myCommentsOnEvent = Comment::whereHas('author', function($query) {
            $query->where('user_id', '=', JWTAuth::parseToken()->toUser()->id);
        })->whereHas('events', function($query) use($request) {
            $query->where('event_id', '=', $request->input('eventId'));
        })->get();

        Log::info($myCommentsOnEvent);

        if($myCommentsOnEvent->isEmpty()) {
            $comment = new Comment();
            $comment->author()->associate(JWTAuth::parseToken()->toUser());
            $comment->message = $request->input('message');
            $comment->save();

            $comment->events()->attach(Event::findOrFail($request->input('eventId')));
            $comment->save();

            // Add attachments
            if($request->exists('attachments')) {
                foreach($request->file('attachments') as $attachment) {
                    $attachmentURL = $this->uploadFile($attachment);

                    if($attachmentURL == null) {
                        return response()->json([
                            'success' => true
                        ], 400);
                    } else {
                        $attachment = new Attachment();
                        $attachment->uploader()->associate(JWTAuth::parseToken()->toUser());
                        $attachment->url = $attachmentURL;
                        $attachment->save();

                        $comment->attachments()->attach($attachment);
                        $comment->save();
                    }
                }
            }

            return response()->json([
                'success' => true,
                'comment' => $comment
            ], 200);
        } else {
            $myCommentsOnEvent[0]->message = $request->input('message');
            $myCommentsOnEvent[0]->save();

            return response()->json([
                'success' => true
            ], 200);
        }
    }

    public function saveProfile(Request $request) {
        $data = $request->all();
        Log::info('saveProfile');
        Log::info($data);

        $user = JWTAuth::parseToken()->toUser();
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->interests = $data['interests'];

        if($data['changePassword'] == 'true') {
            $user->password = bcrypt($data['newPassword']);
        }

        if($request->hasFile('file')) {
            $attachment = new Attachment();
            $attachment->url = $this->uploadFile($data['file']);
            $attachment->uploader()->associate($user);
            $attachment->save();

            $user->profile_icon = $attachment->url;
        }

        $user->save();

        return response()->json([
            'success' => true,
            'editedUser' => $user
        ], 200);
    }

	private function uploadFile(UploadedFile $file){
        $user = JWTAuth::parseToken()->toUser();

        $destinationPath = 'attachments';

        $extension = $file->getClientOriginalExtension();

        $fileName = uniqid() . str_random(20) . '.' . $extension;

        $attachmentUrl = $destinationPath .'/'. $fileName;

        $upload_success = $file->move($destinationPath, $fileName);

        if($upload_success) {
            return $attachmentUrl;
        }

        return null;
    }

	public function test() {
		Log::info('aaaa');
		event(new JoinEvent(User::all()->first()));
	}
}
