<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function author() {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function events() {
    	return $this->belongsToMany('App\Event', 'comment_on_event', 'comment_id', 'event_id')
    		->withTimestamps();
    }

    public function attachments() {
        return $this->belongsToMany('App\Attachment', 'attachment_on_comment', 'comment_id', 'attachment_id')
        	->withTimestamps();
    }
}
