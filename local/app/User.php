<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = ['firstname', 'lastname', 'email', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['interests'];

    public function created_events() {
        return $this->hasMany('App\Event', 'user_id');
    }

    public function events() {
        return $this->belongsToMany('App\Event', 'user_on_event', 'user_id', 'event_id')
            ->withTimestamps();
    }

    public function comments() {
        return $this->hasMany('App\Event', 'user_id');
    }

    public function attachments() {
        return $this->hasMany('App\Attachment', 'user_id');
    }

    public function ratings() {
        return $this->hasMany('App\Rating', 'user_id');
    }

    public function getInterestsAttribute()
    {
        if($this->interests_status == 'public') {
            return $this->attributes['interests'];
        }
        return null;
    }
}
