<?php

namespace App\Listeners;

use App\Events\RatingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RatingEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RatingEvent  $event
     * @return void
     */
    public function handle(RatingEvent $event)
    {
        //
    }
}
