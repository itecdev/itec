<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function owner() {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function participants() {
    	return $this->belongsToMany('App\User', 'user_on_event', 'event_id', 'user_id')
            ->withTimestamps();
    }

    public function comments() {
    	return $this->belongsToMany('App\Comment', 'comment_on_event', 'event_id', 'comment_id')
            ->withTimestamps();
    }

    public function attachments() {
        return $this->belongsToMany('App\Attachment', 'attachment_on_event', 'event_id', 'attachment_id')
            ->withTimestamps();
    }

    public function ratings() {
        return $this->belongsToMany('App\Rating', 'rating_on_event', 'event_id', 'rating_id')
            ->withTimestamps();
    }
}
