<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    public function uploader() {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
