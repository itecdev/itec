<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Rating;

class RatingEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $rating;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($rating)
    {
        //
        $this->rating = $rating;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['real-time-event'];
    }
}
