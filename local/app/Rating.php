<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function events() {
        return $this->belongsToMany('App\Event', 'rating_on_event', 'rating_id', 'event_id')
        	->withTimestamps();
    }
}
